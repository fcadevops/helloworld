# GCC support can be specified at major, minor, or micro version
# (e.g. 8, 8.2 or 8.2.0).
# See https://hub.docker.com/r/library/gcc/ for all supported GCC
# tags from Docker Hub.
# See https://docs.docker.com/samples/library/gcc/ for more on how to use this image
FROM gcc:latest

RUN apt-get -y update && apt-get install -y
RUN apt-get -y install cmake

# These commands copy your files into the specified directory in the image
# and set that as the working location
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp

RUN cmake -E make_directory build
RUN cmake -E chdir build cmake ../CMake 
RUN cmake --build build

# This command runs your application, comment out this line to compile only
CMD ["build/bin/helloworld"]

LABEL Name=helloworld Version=0.0.1
